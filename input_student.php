<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký </title>
    <link rel="stylesheet" href="./styles/input_student.css">
</head>
<body>
    <div class="container">
        <div id="errorMessage"></div>

        <form action="regist_student.php" method="post">
            <!--  -->
            <div class="form-element">
                <label for="inputname" class="input_name">Họ và tên <span class="required">*</span></label>
                <input type="text" name="inputname" class="entering" required>
            </div>

            <!--  -->
            <div class="form-element">
                <label for="gioitinh" class="input_name input_gen">Giới tính <span class="required">*</span></label>
                <div class="choose_gender">
                    <label for="male">
                        <input type="radio" id="male" name="gender" value="Nam" required> Nam
                    </label>
                    <label for="female">
                        <input type="radio" id="female" name="gender" value="Nữ" required> Nữ
                    </label>
                </div>
            </div>

            <!--  -->
            <div class="form-element">
                <label for="ngaysinh" class="input_name date_of_birth">Ngày sinh<span class="required">*</span></label>
                <select name="nam" id="nam" required>
                    <option value="">Năm</option>
                </select>

                <select name="thang" id="thang" required>
                    <option value="">Tháng</option>
                </select>

                <select name="ngay" id="ngay" required>
                    <option value="">Ngày</option>
                </select>
            </div>

            <!--  -->
            <div class="form-element">
                <label for="diachi" class="input_name address">Địa chỉ<span class="required">*</span></label>
                <select name="thanhpho" id="thanhpho" required>
                    <option value="">Thành phố</option>
                </select>

                <select name="quan" id="quan" required>
                    <option value="">Quận</option>
                </select>
            </div>

            <!--  -->
            <div class="form-element">
                <label for="thongtin" class="input_name info">Thông tin khác</label>
                <textarea id="info" name="info" class="input_info" rows="4" cols="30"></textarea>
            </div>

            <!--  -->
            <div class="button-submit">
                <button type="submit" class="button-container" id="submitButton"> Đăng ký </button>
            </div>
        </form>

        <script type="text/javascript" src="./scripts/input.js"></script>
    </div>
</body>
</html>