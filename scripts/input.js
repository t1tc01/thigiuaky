//

//Xu ly ngay thang nam sinh
var currentYear = new Date().getFullYear();
var startYear = currentYear - 40;
var endYear = currentYear - 15;

var namSelect = document.getElementById("nam");
var thangSelect = document.getElementById("thang");
var ngaySelect = document.getElementById("ngay");

for (var year = startYear; year <= endYear; year++) {
  var option = document.createElement("option");
  option.value = year;
  option.text = year;
  namSelect.appendChild(option);
}

for (var month = 1; month <= 12; month++) {
  var option = document.createElement("option");
  option.value = month;
  option.text = month;
  thangSelect.appendChild(option);
}

function updateDays() {
  var selectedYear = parseInt(namSelect.value);
  var selectedMonth = parseInt(thangSelect.value);
  ngaySelect.innerHTML = "";

  var daysInMonth = new Date(selectedYear, selectedMonth, 0).getDate();
  for (var day = 1; day <= daysInMonth; day++) {
    var option = document.createElement("option");
    option.value = day;
    option.text = day;
    ngaySelect.appendChild(option);
  }
}

namSelect.addEventListener("change", updateDays);
thangSelect.addEventListener("change", updateDays);

//Xu ly chon Dia chi
var cities = ["Hà Nội", "Thành phố Hồ Chí Minh"]
var districts_hanoi = ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"]
var districts_hcm = ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"]

var citySelect = document.getElementById("thanhpho");
var districtSelect = document.getElementById("quan");

for (var i = 0; i < cities.length; i++) {
    var option = document.createElement("option");
    option.value = cities[i];
    option.text = cities[i];
    citySelect.appendChild(option);
}

function updateDistricts() {
    var selectedCity = citySelect.value
    districtSelect.innerHTML = ""

    if (selectedCity === "Hà Nội") {
        for (var i = 0; i < districts_hanoi.length; i++) {
            var option = document.createElement("option");
            option.value = districts_hanoi[i];
            option.text = districts_hanoi[i];
            districtSelect.appendChild(option);
        }
    } else if (selectedCity === "Thành phố Hồ Chí Minh") {
        for (var i = 0; i < districts_hcm.length; i++) {
            var option = document.createElement("option");
            option.value = districts_hcm[i];
            option.text = districts_hcm[i];
            districtSelect.appendChild(option);
        }
    }
}

citySelect.addEventListener("change", updateDistricts)

//Xu ly submit button

if (document.getElementById('submitButton') !== null) {
    document.getElementById('submitButton').addEventListener('click', function() {
        //
        var inputName = document.querySelector('.entering');
        
        //
        var genderInputs = document.querySelectorAll('input[name="gender"]');
        var selectedGender = Array.from(genderInputs).find(input => input.checked);
        var gender = ""
        genderInputs.forEach(function(input) {
            if (input.checked) {
                gender = input.value;
            }
        });

        //
        var namSelect = document.getElementById("nam");
        var thangSelect = document.getElementById("thang");
        var ngaySelect = document.getElementById("ngay");
        
        selectedYear = namSelect.value;
        selectedMonth = thangSelect.value;
        selectedDay = ngaySelect.value;

        //
        var citySelect = document.getElementById("thanhpho");
        var districtSelect = document.getElementById("quan");

        selectedCity = citySelect.value;
        selectedDistrict = districtSelect.value;
        
        //
        var errorMessage = document.getElementById('errorMessage');
        errorMessage.innerHTML = ''; 

        if (inputName.value.trim() === '') {
            errorMessage.innerHTML += 'Hãy nhập họ tên.<br>';
        }
        
        if (!selectedGender) {
            errorMessage.innerHTML += 'Hãy chọn giới tính <br>';
        }

        if (selectedYear === '' || selectedMonth === '' || selectedDay === '') {
            errorMessage.innerHTML += 'Hãy chọn ngày sinh.<br>';
        }

        if (selectedCity === '' || selectedDistrict === '') {
            errorMessage.innerHTML += 'Hãy chọn địa chỉ.<br>';
        }

        if (errorMessage.innerHTML !== '') {
            errorMessage.style.display = 'block';
            console.log("Đăng ký lỗi!")
        } 
        else {
            errorMessage.style.display = 'none';

            console.log("Đăng ký thành công!");
        }
    });
}



