<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thông tin đăng ký</title>
    <link rel="stylesheet" href="./styles/regist_student.css">
</head>
<body>
    <div class="container">
    <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $name = $_POST["inputname"]; 
            $gender = $_POST["gender"];
            
            $birthYear = $_POST["nam"];
            $birthMonth = $_POST["thang"];
            $birthDay = $_POST["ngay"];

            $city = $_POST["thanhpho"];
            $district = $_POST["quan"];

            $info = $_POST["info"];

            echo '<p>Họ tên: ' . $name . '</p>';
            echo '<p>Giới tính: ' . $gender . '</p>';
            echo '<p>Ngày sinh: ' . $birthDay . '/'. $birthMonth . '/'. $birthYear.'</p>';
            echo '<p>Địa chỉ: ' . $district . ' - '. $city .'</p>';
            echo '<p>Thông tin khác: ' . $info . '</p>';
        }
    ?>
    </div>
</body>
</html>